<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginCont extends Controller
{
    public function index(){
        return view('back.login');
    }

    public function login(Request $req){
        $validator = app('validator')->make($req->all(),[
            'email' => 'required',
            'password' => 'required|min:7'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator->messages());
        }

        if(auth('admin')->attempt(['email'=>$req->email,'password'=>$req->password])){
            return redirect()->route('bo.dashboard');
        }else if(auth('admin')->attempt(['username'=>$req->email,'password'=>$req->password])){
            return redirect()->route('bo.dashboard');
        }

        return redirect()->back()->withErrors(['failed'=>'Email/password tidak benar']);
    }

    public function logout(){
        auth('admin')->logout();
        session()->flush();
        return redirect()->route('bo.login')->withErrors(['failed'=>'Logout berhasil']);
    }
}
